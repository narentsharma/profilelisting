var GMaps = require('gmaps');

(function(){
    var profileList = new ProfileList();

    //class to create profiles
    function UserProfile(objProfile) {
        this.id = objProfile.id;
        this.name = objProfile.name;
        this.username = objProfile.username;
        this.email = objProfile.email;
        this.address = objProfile.address;
        this.phone = objProfile.phone;
        this.website = objProfile.website;
        this.company = objProfile.company;        
    }

    //class to create profiles list
    function ProfileList() {
        this.list = [];
        this.addProfile = function (cont) {
            this.list.push(cont);
        }
    }

    //get all user profiles
    function getProfiles(method, url, callback) {
        var xhr = new XMLHttpRequest();    
        xhr.onreadystatechange = function (e) {
            if (this.readyState == 4 && this.status == 200) {
                callback(this.responseText);
            }
        };
        xhr.open(method, url, true);
        xhr.send();
    }    

    //parse all user profile row data to list items
    function processProfilesList(res) {
        var profiles = JSON.parse(res);    
        profiles.forEach(function(profile) {
            profileList.list.push(new UserProfile(profile));
        });
        appendProfileList(profileList.list);        
    }
    
    //creates HMTL dom element
    function createNode(type, classes, innerHTML) {
        var node = document.createElement(type);
        if(classes) node.className = typeof(classes) === 'string' ? classes : classes.join(' ');
        node.innerHTML = innerHTML ? innerHTML : '';
        return node;   
    }

    //creates prfile cards for all items in ProfileList
    function appendProfileList(profileList) {
        sortProfileList();
        var listEle = document.getElementById('profileListCon');
        listEle.innerHTML = '';
        profileList.forEach(function(cont, index) {     
            if((cont.id % 2) == 0) {             
                var listItemWraper = createNode('div', 'con-25 con-md-33 con-sm-50 con-xs-100');
                var listItem = createNode('div', 'profile-card');                      
                    
                listItem.appendChild(createNode('div', 'con-100', '<label>ID: </label>' + cont.id));
                listItem.appendChild(createNode('div', 'con-100', '<label>NAME: </label>' + cont.name));
                listItem.appendChild(createNode('div', 'con-100', '<label>USERNAME: </label>' + cont.username));
                listItem.appendChild(createNode('div', 'con-100', '<label>STREET: </label>' + cont.address.street));
                listItem.appendChild(createNode('div', 'con-100', '<label>SUITE: </label>' + cont.address.suite));
                listItem.appendChild(createNode('div', 'con-100', '<label>CITY: </label>' + cont.address.city));
                listItem.appendChild(createNode('div', 'con-100', '<label>ZIPCODE: </label>' + cont.address.zipcode));
                listItem.appendChild(createNode('div', 'con-100', '<label>PHONE: </label>' + cont.phone));
                listItem.appendChild(createNode('div', 'con-100', '<label>WEBSITE: </label>' + cont.website));
                listItem.appendChild(createNode('div', 'con-100', '<label>COMPANY: </label>' + cont.company.name));

                var mapItem = createNode('div', 'map-container');
                mapItem.id = 'map' + cont.id;
                listItem.appendChild(mapItem);
                listItemWraper.appendChild(listItem);
                listEle.appendChild(listItemWraper);

                var map = new GMaps({
                    el: '#' + mapItem.id,                    
                    lat: cont.address.geo.lat,
                    lng: cont.address.geo.lng,                    
                    zoom: 2
                });

                map.addMarker({
                    lat: cont.address.geo.lat,
                    lng: cont.address.geo.lng,
                    title: cont.name                    
                });                
            }
        });        
    }

    //sorts profile list by last name of user
    function sortProfileList() {
        profileList.list = profileList.list.sort(function(a, b) { 
            var arrA = a.name.split(' ');
            var arrB = b.name.split(' ');
            if (arrA[arrA.length - 1] < arrB[arrB.length - 1]) {
                return -1;
            }
            if (arrA[arrA.length - 1] > arrB[arrB.length - 1]) {
                return 1;
            }        
            return 0;
        });    
    } 
    
    getProfiles('GET', 'https://jsonplaceholder.typicode.com/users', processProfilesList);
})();